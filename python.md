##virtualenv
###install

```base 
pip install virtualenv #install
virtualenv -p python3 venvname	#创建虚拟环境
source venvname/bin/activate  #激活虚拟环境
deactivate #退出虚拟环境

```

### linux install package
```bash
python3.6 -m pip install
```

### requirements.txt
```bash
pip freeze > requirements.txt   	#生成requirements.txt
pip install -r requirements.txt		#安装依赖包
```

###setup.py

```python
```
